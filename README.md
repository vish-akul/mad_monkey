# Mad Monkey

All you need to run the fuzzer is docker and docker-compose. Simply do:

```bash
docker-compose build
```

to build the image. And:

```bash
docker-compose up
```

to run the fuzzer.

The inputs that crash and corresponding logs can be found in the `outputs` directory in the host itself.

A TL;DR for usage can be found in `USAGE.md`
Patches can be applied to libalias by modifying the code in `libalias-linux`. The image can then be simply rebuilt and fuzzing can be continued.
As the version of libalias we are fuzzing is originally written and maintained for FreeBSD. I have patched the library so that it can be run and fuzzed on a linux system, without affecting its functionality.