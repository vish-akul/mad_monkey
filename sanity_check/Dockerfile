FROM ubuntu:16.04
WORKDIR /usr/fuzz/
RUN apt-get update && apt-get --yes install \
        curl subversion screen gcc g++ cmake ninja-build golang autoconf libtool \
        apache2 python-dev pkg-config zlib1g-dev libgcrypt11-dev libgss-dev libssl-dev \
        libxml2-dev ragel nasm libarchive-dev make automake libdbus-1-dev libboost-dev \
        autoconf-archive libbsd-dev python-pip

ENV CLANG_VERSION=9.0.0
ENV CLANG_DIR=clang+llvm-$CLANG_VERSION-x86_64-linux-gnu-ubuntu-16.04
RUN curl https://releases.llvm.org/$CLANG_VERSION/$CLANG_DIR.tar.xz | tar xfJ - \
    && rm -rf /usr/local/bin/clang* /usr/local/lib/clang \
    && cp -rf  $CLANG_DIR/bin/*  /usr/local/bin \
    && cp -rf  $CLANG_DIR/lib/clang  /usr/local/lib \
    && rm -rf $CLANG_DIR \
    && pip install scapy \
    && svn co http://llvm.org/svn/llvm-project/compiler-rt/trunk/lib/fuzzer Fuzzer 

COPY libalias-vuln ./libalias-vuln/.

RUN cd /usr/fuzz/libalias-vuln \
    && ./gen_conf.sh \
    && ./build_coverage.sh \
    && make clean \
    && make \
    && python extract.py pcaps/ seeds/ 

WORKDIR /usr/fuzz/outputs
ENV PATH=$PATH:$PWD
CMD ["./runFuzz"]
