# USAGE

docker and docker-compose are required.
To build and start fuzzing use:
```
docker-compose build
docker-compose up
```

Logs, Crash Reports and crashing inputs will show up in `outputs` directory.
Coverage can be viewed using:
```
docker-compose run mad_monkey show_cov
```

Run with a input or test a crashing test case using:
```
docker-compose run mad_monkey test_input <input file path relative to outputs dir>
```
