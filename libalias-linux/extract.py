from scapy.all import *
from os import listdir
from os.path import isfile, join
import sys

def get_files(path):
    onlyfiles = [f for f in listdir(path) if isfile(join(path, f))]
    return onlyfiles

inpath = sys.argv[1]
outpath = sys.argv[2]

files = get_files(inpath)

for pcap in files:
    print pcap
    pcap_file = join(inpath, pcap)
    # rdpcap comes from scapy and loads in our pcap file
    packets = rdpcap(pcap_file)

    # Let's iterate through every packet
    for num, packet in enumerate(packets):
        filename = "%s-%d.bin" % (pcap, num)
        raw = join(outpath, filename)
        r = open(raw, 'w')
        r.write(str(packet)[14:-4])
        # r.write(str(packet))
        r.close()
