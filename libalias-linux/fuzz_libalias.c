#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include <stdlib.h>

#include <sys/types.h>
#include <netinet/in.h>
#include <alias.h>

static struct libalias *la;

int LLVMFuzzerInitialize() {
	la = LibAliasInit(la);
	LibAliasRefreshModules();
	return 0;
}

int LLVMFuzzerTestOneInput(const uint8_t *Data, size_t Size) {
	// Hackish fixes to prevent crashing on really shallow bugs
	if (Size < 40) {
		return 0;
	}
	// Make sure IP Packet length chack is proper
	if ((Data[0] & 0xf) != 0x05) {
		return 0;
	}
	// Enforce length on ICMP Packets
	if (Data[9] == 0x01) {
		if (Size < 200) {
			return 0;
		}
	}
	char * pkt = malloc(Size);
	memcpy(pkt, Data, Size);
	LibAliasIn(la, pkt, (int) Size);

	memcpy(pkt, Data, Size);
	LibAliasOut(la, pkt, (int) Size);
	free(pkt);
 	return 0;
}
